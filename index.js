
//Activity:
/*// #1:
const arrayName = ["arrayName1", "arrayName2", "arrayName3", "arrayName4"];
console.log(arrayName);

// #2:

console.log(arrayName[arrayName.length-1]);

// #3:
console.log(arrayName[arrayName.length-1]);

// #4:
console.log(arrayName.indexOf('arrayName2'));

console.log(arrayName.indexOf('arrayName0'));



// #5:
arrayName.forEach()


//#6:
arrayName.map()

// #7:
arrayName.every()

// #8:
arrayName.some()

// #9:
false

// #10:
true*/



// Activity - Function Coding
// #1

/*let students = ["John", "Joe", "Jane", "Jessica"];

function addToEnd(students, student){
	if(typeof student === 'string'){
		students.push(student);
		return students;
	}
	else{
		return `error - can only add strings to an array`
	}
}



// #2

function addToStart(students, student){
	if(typeof student === 'string'){
		students.unshift(student);
		return students;
	}
	else{
		return `error - can only add strings to an array`
	}
}

*/

// #3

const elementChecker = (arr, elementToBeChecked) => {
	if(arr.length === 0){
		return "error - passed in array is empty";
	}

	return arr.some(element => element === elementToBeChecked);	
}

console.log(elementChecker(students, "Jane"));

// #4
const checkAllStringsEnding = (arr, char) => {
	if(arr.length === 0) return "error - array must NOT be empty";

	if(arr.some(element => typeof element !== "string")) return "error -all array elements must be strings";

	if(char.length > 1) return "error - 2nd argument must be a single character";

	if(char.length > 1) return "error - 2nd argument must be a single character";

	return arr.every(element => element[element.length-1] === char);
}
console.log(checkAllStringsEnding(students, "e"));
console.log(checkAllStringsEnding([], "e"));
console.log(checkAllStringsEnding(["Jane", 2], "e"))
console.log(checkAllStringsEnding(students, 4));
console.log(checkAllStringsEnding(students, "el"));



// #5

const stringLengthSorter = (arr) => {
	if(arr.some(element => typeof element !== "string"))
		return "error - all array elements must be strings";
	arr.sort((elementA, elementB) => {
		return elementA.length - elementB.length;
	})
	return arr;
}

console.log(stringLengthSorter(students));
console.log(stringLengthSorter([37, "John", 39, "Jane"]))